"""Chunk splitter.

This small script splits a set of BGT GML files in smaller chunks.

It relies on the fact that the GML of the BGT is serialized with
all XML tags for 1 feature on 1 line, so we can split the file based
on line ends, without having to interpret the GML file, at all.

However, after splitting, we need to make sure that the root tag
surrounding the original document is also present in the smaller
chunks. We add the root start and end tag, if it is not present,
in the chunk.

"""

import os
import glob
from math import ceil

ROOT_START = """<?xml version='1.0' encoding='UTF-8'?><CityModel xmlns:gml='http://www.opengis.net/gml' xmlns:imgeo='http://www.geostandaarden.nl/imgeo/2.1' xmlns='http://www.opengis.net/citygml/2.0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.geostandaarden.nl/imgeo/2.1 http://schemas.geonovum.nl/imgeo/2.1/imgeo-2.1.1.xsd'>""" # make sure that no quotes other than ' are used (due to echo)

ROOT_END = """</CityModel>"""

def as_gb(size):
    return size * 1024 * 1024 * 1024

# thresholds in GB
SIZE_TOO_LARGE = as_gb(7.5)
CHUNK_SIZE_WANTED = as_gb(2.5)


def roundup(x, multiple=100000.0):
    "Rounds up integer to multiple"
    return int(ceil(x / multiple) * multiple)


def split(filenm):
    """Uses Unix split to split text file in smaller pieces.

    The number of lines and the size of the original file are obtained
    to guess what would be a good number of lines to put in every chunk
    not to exceed the CHUNK_SIZE_WANTED. There is no guarantee however
    that the resulting chunks will not be bigger than this size
    (actually some will be bigger, dependent on how large lines are
    distributed in the file).
    """
    bytes = os.path.getsize(filenm)
    orig_lines = int(os.popen('wc -l ' + filenm).read().split(' ')[0])
    byte_per_line = bytes / orig_lines
    lines = roundup(CHUNK_SIZE_WANTED / byte_per_line)
    print('splitting ' + filenm + ', size: ' + str(bytes) + " with "+ str(orig_lines) + " lines; into chunks with lines: " + str(lines))
    basename, ext = os.path.splitext(os.path.basename(filenm))
    os.popen("split -l " + str(lines) + " " + filenm + " chunk_" + basename + "- --additional-suffix=" + ext)
    os.popen('rm ' + filenm)


def fix_chunk(filenm):
    """Fix the root tags surrounding a chunk file.

    A chunk can have a correct start or end root tag, or it can be missing
    due to the splitting on line ends. If a tag is missing, we add it
    to the file.

    This function makes a temporary copy of the chunk file.
    """
    print('fixing root tags for ' + filenm)
    first_line = os.popen('head -1 ' + filenm).read()
    last_line = os.popen('tail -n 1 ' + filenm).read()
    new_filenm = 'tmp_' + filenm
    os.popen('touch ' + new_filenm)
    if not first_line.startswith(ROOT_START[:5]):
        os.popen('echo "' + ROOT_START + '" > ' + new_filenm)
    os.popen('cat ' + filenm + ' >> ' + new_filenm)
    if not last_line.startswith(ROOT_END[:5]):
        os.popen('echo "' + ROOT_END + '" >> ' + new_filenm)
    os.popen('rm ' + filenm)
    os.popen('mv ' + new_filenm + ' ' + filenm)


def split_wrap(filenm):
    size = os.path.getsize(filenm)
    if size > SIZE_TOO_LARGE:
        split(filenm)


def process_files():
    """Main function, process all GML files and split those that are too
    large (according to the SIZE_TOO_LARGE threshold).
    """
    from multiprocessing import Pool
    pool = Pool(processes=4) # start 4 worker processes
    pool.map(split_wrap, glob.glob('bgt_*.gml'))
    pool.map(fix_chunk, glob.glob('chunk_*.gml'))


if __name__ == "__main__":
    process_files()
