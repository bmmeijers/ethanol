#!/bin/bash

# Use from the Xerces project the sample program SAX2Count to be able to 
# validate the GML against the schema
# https://xerces.apache.org/xerces-c/sax2count-3.html

for i in `ls *.gml`; do SAX2Count $i; done
