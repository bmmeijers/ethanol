#!/bin/bash

timestamp() {
  local d=`date +"%T"`
  echo '--' $d '--' $1
}

### PHASE -- Download / unzip
#
#timestamp 'begin download'
#wget https://downloads.pdok.nl/service/cache/bgt-citygml-nl-nopbp.zip
#
#timestamp 'begin unpack'
#unzip bgt-citygml-nl-nopbp.zip

timestamp 'begin load'
ogr2ogr --version

### PHASE -- Prepare files for schema definition
echo '<?xml version="1.0" encoding="UTF-8"?><gml:FeatureCollection xmlns:gml="http://www.opengis.net/gml"><gml:featureMember><object /></gml:featureMember></gml:FeatureCollection>' > _prepare.gml
cp imgeo-v2.1.1-stroked.gfs _prepare.gfs
ogr2ogr -nlt CONVERT_TO_LINEAR -dim XY --config PG_USE_COPY YES -f "PGDUMP" _prepare.sql _prepare.gml -lco SPATIAL_INDEX=ON -lco SCHEMA=martijn 
# -lco GEOMETRY_NAME=geometrie ## On our server the column name for featureclass with just 1 geometry column is not picked up properly by geompropertydefn, even if it is given in the  (maybe was fixed in later release of ogr2ogr)

# split out in non-spatial index / spatial index:
# lines that do *not* contain USING GIST
grep -v "USING GIST" _prepare.sql > create-schema.sql
# lines that do contain USING GIST
grep "USING GIST" _prepare.sql > create-index.sql

# remove temp files produced by the above
rm _prepare.gml
rm _prepare.gfs
rm _prepare.sql

### PHASE -- Split files
#timestamp 'begin chunk-splitter'
#python chunk-splitter.py

### PHASE -- Conversion of GML to PGDUMP
timestamp 'begin conversion'

# do the hard work: conversion from GML to PGDUMP format
find . -name '*.gml' | parallel cp imgeo-v2.1.1-stroked.gfs {.}.gfs
# make sure gfs files are newer than gml files
touch *.gml
sleep 2
touch *.gfs
# convert the gml file
# FIXME: the geometry for some feature classes should be multi polygons (bgt_pand), this can be accomplished by passing
# -nlt PROMOTE_TO_MULTI 
# to the ogr2ogr command -- maybe we could rename these files to multi_bgt_*.gml and run them in a separate pass
find . -name '*.gml' | parallel ogr2ogr -nlt CONVERT_TO_LINEAR -dim XY -f "PGDUMP" {.}.sql --config PG_USE_COPY YES {} -lco SPATIAL_INDEX=NO -lco CREATE_TABLE=NO -lco SCHEMA=martijn -skipfailures -append -update

### PHASE -- Create schema in the database
timestamp 'loading schema'

# load: create schema
psql -d geodata -f create-schema.sql

### PHASE -- Load data to the database
timestamp 'loading data'

# load: data
find . -name 'bgt_*.sql' | parallel psql -d geodata -f {}
find . -name 'chunk_*.sql' | parallel psql -d geodata -f {}

### PHASE -- Create geometry indexes
timestamp 'loading indexes'
# make sure that the multiple of the number of lines is corresponding with how split is instructed to split the files (below: "-lX" argument)
# FIXME: for processing it would be better to have create index for large tables in separate files (as these index creation processes will take long; better to start early and in parallel)
echo 'SET default_tablespace=INDX;
SET maintenance_work_mem=536870912; -- 512MB
CREATE INDEX "wegdeel_geometrie_vlak_geom_idx" ON "martijn"."bgt_wegdeel" USING GIST ("geometrie_vlak");
CREATE INDEX "wegdeel_geometrie_kruinlijn_geom_idx" ON "martijn"."bgt_wegdeel" USING GIST ("geometrie_kruinlijn");
CREATE INDEX "ondersteunendwegdeel_geometrie_vlak_geom_idx" ON "martijn"."bgt_ondersteunendwegdeel" USING GIST ("geometrie_vlak");
CREATE INDEX "ondersteunendwegdeel_geometrie_kruinlijn_geom_idx" ON "martijn"."bgt_ondersteunendwegdeel" USING GIST ("geometrie_kruinlijn");
CREATE INDEX "onbegroeidterreindeel_geometrie_vlak_geom_idx" ON "martijn"."bgt_onbegroeidterreindeel" USING GIST ("geometrie_vlak");
CREATE INDEX "onbegroeidterreindeel_geometrie_kruinlijn_geom_idx" ON "martijn"."bgt_onbegroeidterreindeel" USING GIST ("geometrie_kruinlijn");
CREATE INDEX "begroeidterreindeel_geometrie_vlak_geom_idx" ON "martijn"."bgt_begroeidterreindeel" USING GIST ("geometrie_vlak");

SET maintenance_work_mem=536870912; -- 512MB
SET default_tablespace=INDX;
CREATE INDEX "begroeidterreindeel_geometrie_kruinlijn_geom_idx" ON "martijn"."bgt_begroeidterreindeel" USING GIST ("geometrie_kruinlijn");
CREATE INDEX "waterdeel_wkb_geometry_geom_idx" ON "martijn"."bgt_waterdeel" USING GIST ("wkb_geometry");
CREATE INDEX "ondersteunendwaterdeel_wkb_geometry_geom_idx" ON "martijn"."bgt_ondersteunendwaterdeel" USING GIST ("wkb_geometry");
CREATE INDEX "pand_geometrie_vlak_geom_idx" ON "martijn"."bgt_pand" USING GIST ("geometrie_vlak");
CREATE INDEX "pand_geometrie_nummeraanduiding_geom_idx" ON "martijn"."bgt_pand" USING GIST ("geometrie_nummeraanduiding");
CREATE INDEX "spoor_wkb_geometry_geom_idx" ON "martijn"."bgt_spoor" USING GIST ("wkb_geometry");
CREATE INDEX "overigbouwwerk_wkb_geometry_geom_idx" ON "martijn"."bgt_overigbouwwerk" USING GIST ("wkb_geometry");

SET default_tablespace=INDX;
SET maintenance_work_mem=536870912; -- 512MB
CREATE INDEX "kunstwerkdeel_wkb_geometry_geom_idx" ON "martijn"."bgt_kunstwerkdeel" USING GIST ("wkb_geometry");
CREATE INDEX "overbruggingsdeel_wkb_geometry_geom_idx" ON "martijn"."bgt_overbruggingsdeel" USING GIST ("wkb_geometry");
CREATE INDEX "tunneldeel_wkb_geometry_geom_idx" ON "martijn"."bgt_tunneldeel" USING GIST ("wkb_geometry");
CREATE INDEX "scheiding_wkb_geometry_geom_idx" ON "martijn"."bgt_scheiding" USING GIST ("wkb_geometry");
CREATE INDEX "overigescheiding_wkb_geometry_geom_idx" ON "martijn"."bgt_overigescheiding" USING GIST ("wkb_geometry");
CREATE INDEX "functioneelgebied_wkb_geometry_geom_idx" ON "martijn"."bgt_functioneelgebied" USING GIST ("wkb_geometry");


SET default_tablespace=INDX;
SET maintenance_work_mem=536870912; -- 512MB
CREATE INDEX "ongeclassificeerdobject_wkb_geometry_geom_idx" ON "martijn"."bgt_ongeclassificeerdobject" USING GIST ("wkb_geometry");
CREATE INDEX "openbareruimtelabel_wkb_geometry_geom_idx" ON "martijn"."bgt_openbareruimtelabel" USING GIST ("wkb_geometry");
CREATE INDEX "plaatsbepalingspunt_wkb_geometry_geom_idx" ON "martijn"."bgt_plaatsbepalingspunt" USING GIST ("wkb_geometry");
CREATE INDEX "gebouwinstallatie_wkb_geometry_geom_idx" ON "martijn"."bgt_gebouwinstallatie" USING GIST ("wkb_geometry");
CREATE INDEX "bak_wkb_geometry_geom_idx" ON "martijn"."bgt_bak" USING GIST ("wkb_geometry");
CREATE INDEX "bord_wkb_geometry_geom_idx" ON "martijn"."bgt_bord" USING GIST ("wkb_geometry");
CREATE INDEX "installatie_wkb_geometry_geom_idx" ON "martijn"."bgt_installatie" USING GIST ("wkb_geometry");

SET default_tablespace=INDX;
SET maintenance_work_mem=536870912; -- 512MB
CREATE INDEX "kast_wkb_geometry_geom_idx" ON "martijn"."bgt_kast" USING GIST ("wkb_geometry");
CREATE INDEX "mast_wkb_geometry_geom_idx" ON "martijn"."bgt_mast" USING GIST ("wkb_geometry");
CREATE INDEX "paal_wkb_geometry_geom_idx" ON "martijn"."bgt_paal" USING GIST ("wkb_geometry");
CREATE INDEX "put_wkb_geometry_geom_idx" ON "martijn"."bgt_put" USING GIST ("wkb_geometry");
CREATE INDEX "sensor_wkb_geometry_geom_idx" ON "martijn"."bgt_sensor" USING GIST ("wkb_geometry");
CREATE INDEX "straatmeubilair_wkb_geometry_geom_idx" ON "martijn"."bgt_straatmeubilair" USING GIST ("wkb_geometry");
CREATE INDEX "waterinrichtingselement_wkb_geometry_geom_idx" ON "martijn"."bgt_waterinrichtingselement" USING GIST ("wkb_geometry");

SET default_tablespace=INDX;
SET maintenance_work_mem=536870912; -- 512MB
CREATE INDEX "weginrichtingselement_wkb_geometry_geom_idx" ON "martijn"."bgt_weginrichtingselement" USING GIST ("wkb_geometry");
CREATE INDEX "vegetatieobject_wkb_geometry_geom_idx" ON "martijn"."bgt_vegetatieobject" USING GIST ("wkb_geometry");
CREATE INDEX "buurt_wkb_geometry_geom_idx" ON "martijn"."bgt_buurt" USING GIST ("wkb_geometry");
CREATE INDEX "openbareruimte_wkb_geometry_geom_idx" ON "martijn"."bgt_openbareruimte" USING GIST ("wkb_geometry");
CREATE INDEX "stadsdeel_wkb_geometry_geom_idx" ON "martijn"."bgt_stadsdeel" USING GIST ("wkb_geometry");
CREATE INDEX "waterschap_wkb_geometry_geom_idx" ON "martijn"."bgt_waterschap" USING GIST ("wkb_geometry");
CREATE INDEX "wijk_wkb_geometry_geom_idx" ON "martijn"."bgt_wijk" USING GIST ("wkb_geometry");
' > _index.sql

# split the file into more files with "-l X" lines per file
split -l 10 _index.sql post-index- --additional-suffix=.sql
# create the indexes per feature type in parallel
find . -name 'post-index-*.sql' | parallel psql -d geodata -f {}

timestamp 'vacuum tables'
echo '
SET maintenance_work_mem=536870912; -- 512MB
VACUUM ANALYZE "martijn"."bgt_wegdeel";
VACUUM ANALYZE "martijn"."bgt_ondersteunendwegdeel";
VACUUM ANALYZE "martijn"."bgt_onbegroeidterreindeel";
VACUUM ANALYZE "martijn"."bgt_begroeidterreindeel";
VACUUM ANALYZE "martijn"."bgt_waterdeel";
VACUUM ANALYZE "martijn"."bgt_ondersteunendwaterdeel";
VACUUM ANALYZE "martijn"."bgt_pand";
VACUUM ANALYZE "martijn"."bgt_spoor";
VACUUM ANALYZE "martijn"."bgt_overigbouwwerk";
VACUUM ANALYZE "martijn"."bgt_kunstwerkdeel";
VACUUM ANALYZE "martijn"."bgt_overbruggingsdeel";
VACUUM ANALYZE "martijn"."bgt_tunneldeel";
VACUUM ANALYZE "martijn"."bgt_scheiding";
VACUUM ANALYZE "martijn"."bgt_overigescheiding";
VACUUM ANALYZE "martijn"."bgt_functioneelgebied";
VACUUM ANALYZE "martijn"."bgt_ongeclassificeerdobject";
VACUUM ANALYZE "martijn"."bgt_openbareruimtelabel";
VACUUM ANALYZE "martijn"."bgt_plaatsbepalingspunt";
VACUUM ANALYZE "martijn"."bgt_gebouwinstallatie";
VACUUM ANALYZE "martijn"."bgt_bak";
VACUUM ANALYZE "martijn"."bgt_bord";
VACUUM ANALYZE "martijn"."bgt_installatie";
VACUUM ANALYZE "martijn"."bgt_kast";
VACUUM ANALYZE "martijn"."bgt_mast";
VACUUM ANALYZE "martijn"."bgt_paal";
VACUUM ANALYZE "martijn"."bgt_put";
VACUUM ANALYZE "martijn"."bgt_sensor";
VACUUM ANALYZE "martijn"."bgt_straatmeubilair";
VACUUM ANALYZE "martijn"."bgt_waterinrichtingselement";
VACUUM ANALYZE "martijn"."bgt_weginrichtingselement";
VACUUM ANALYZE "martijn"."bgt_vegetatieobject";
VACUUM ANALYZE "martijn"."bgt_buurt";
VACUUM ANALYZE "martijn"."bgt_openbareruimte";
VACUUM ANALYZE "martijn"."bgt_stadsdeel";
VACUUM ANALYZE "martijn"."bgt_waterschap";
VACUUM ANALYZE "martijn"."bgt_wijk";' > post-vacuum.sql
nohup psql -d geodata -f post-vacuum.sql &

timestamp 'get statistics on tables'
echo "SELECT * FROM (
select 'bgt_bak' as table, pg_size_pretty(pg_table_size('bgt_bak')) as table_size, pg_size_pretty(pg_indexes_size('bgt_bak')) as indexes_size, count(*) as ct from bgt_bak
 UNION ALL 
select 'bgt_begroeidterreindeel' as table, pg_size_pretty(pg_table_size('bgt_begroeidterreindeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_begroeidterreindeel')) as indexes_size, count(*) as ct from bgt_begroeidterreindeel
 UNION ALL 
select 'bgt_bord' as table, pg_size_pretty(pg_table_size('bgt_bord')) as table_size, pg_size_pretty(pg_indexes_size('bgt_bord')) as indexes_size, count(*) as ct from bgt_bord
 UNION ALL 
select 'bgt_buurt' as table, pg_size_pretty(pg_table_size('bgt_buurt')) as table_size, pg_size_pretty(pg_indexes_size('bgt_buurt')) as indexes_size, count(*) as ct from bgt_buurt
 UNION ALL 
select 'bgt_functioneelgebied' as table, pg_size_pretty(pg_table_size('bgt_functioneelgebied')) as table_size, pg_size_pretty(pg_indexes_size('bgt_functioneelgebied')) as indexes_size, count(*) as ct from bgt_functioneelgebied
 UNION ALL 
select 'bgt_gebouwinstallatie' as table, pg_size_pretty(pg_table_size('bgt_gebouwinstallatie')) as table_size, pg_size_pretty(pg_indexes_size('bgt_gebouwinstallatie')) as indexes_size, count(*) as ct from bgt_gebouwinstallatie
 UNION ALL 
select 'bgt_installatie' as table, pg_size_pretty(pg_table_size('bgt_installatie')) as table_size, pg_size_pretty(pg_indexes_size('bgt_installatie')) as indexes_size, count(*) as ct from bgt_installatie
 UNION ALL 
select 'bgt_kast' as table, pg_size_pretty(pg_table_size('bgt_kast')) as table_size, pg_size_pretty(pg_indexes_size('bgt_kast')) as indexes_size, count(*) as ct from bgt_kast
 UNION ALL 
select 'bgt_kunstwerkdeel' as table, pg_size_pretty(pg_table_size('bgt_kunstwerkdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_kunstwerkdeel')) as indexes_size, count(*) as ct from bgt_kunstwerkdeel
 UNION ALL 
select 'bgt_mast' as table, pg_size_pretty(pg_table_size('bgt_mast')) as table_size, pg_size_pretty(pg_indexes_size('bgt_mast')) as indexes_size, count(*) as ct from bgt_mast
 UNION ALL 
select 'bgt_onbegroeidterreindeel' as table, pg_size_pretty(pg_table_size('bgt_onbegroeidterreindeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_onbegroeidterreindeel')) as indexes_size, count(*) as ct from bgt_onbegroeidterreindeel
 UNION ALL 
select 'bgt_ondersteunendwaterdeel' as table, pg_size_pretty(pg_table_size('bgt_ondersteunendwaterdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_ondersteunendwaterdeel')) as indexes_size, count(*) as ct from bgt_ondersteunendwaterdeel
 UNION ALL 
select 'bgt_ondersteunendwegdeel' as table, pg_size_pretty(pg_table_size('bgt_ondersteunendwegdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_ondersteunendwegdeel')) as indexes_size, count(*) as ct from bgt_ondersteunendwegdeel
 UNION ALL 
select 'bgt_ongeclassificeerdobject' as table, pg_size_pretty(pg_table_size('bgt_ongeclassificeerdobject')) as table_size, pg_size_pretty(pg_indexes_size('bgt_ongeclassificeerdobject')) as indexes_size, count(*) as ct from bgt_ongeclassificeerdobject
 UNION ALL 
select 'bgt_openbareruimte' as table, pg_size_pretty(pg_table_size('bgt_openbareruimte')) as table_size, pg_size_pretty(pg_indexes_size('bgt_openbareruimte')) as indexes_size, count(*) as ct from bgt_openbareruimte
 UNION ALL 
select 'bgt_openbareruimtelabel' as table, pg_size_pretty(pg_table_size('bgt_openbareruimtelabel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_openbareruimtelabel')) as indexes_size, count(*) as ct from bgt_openbareruimtelabel
 UNION ALL 
select 'bgt_overbruggingsdeel' as table, pg_size_pretty(pg_table_size('bgt_overbruggingsdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_overbruggingsdeel')) as indexes_size, count(*) as ct from bgt_overbruggingsdeel
 UNION ALL 
select 'bgt_overigbouwwerk' as table, pg_size_pretty(pg_table_size('bgt_overigbouwwerk')) as table_size, pg_size_pretty(pg_indexes_size('bgt_overigbouwwerk')) as indexes_size, count(*) as ct from bgt_overigbouwwerk
 UNION ALL 
select 'bgt_overigescheiding' as table, pg_size_pretty(pg_table_size('bgt_overigescheiding')) as table_size, pg_size_pretty(pg_indexes_size('bgt_overigescheiding')) as indexes_size, count(*) as ct from bgt_overigescheiding
 UNION ALL 
select 'bgt_paal' as table, pg_size_pretty(pg_table_size('bgt_paal')) as table_size, pg_size_pretty(pg_indexes_size('bgt_paal')) as indexes_size, count(*) as ct from bgt_paal
 UNION ALL 
select 'bgt_pand' as table, pg_size_pretty(pg_table_size('bgt_pand')) as table_size, pg_size_pretty(pg_indexes_size('bgt_pand')) as indexes_size, count(*) as ct from bgt_pand
 UNION ALL 
select 'bgt_plaatsbepalingspunt' as table, pg_size_pretty(pg_table_size('bgt_plaatsbepalingspunt')) as table_size, pg_size_pretty(pg_indexes_size('bgt_plaatsbepalingspunt')) as indexes_size, count(*) as ct from bgt_plaatsbepalingspunt
 UNION ALL 
select 'bgt_put' as table, pg_size_pretty(pg_table_size('bgt_put')) as table_size, pg_size_pretty(pg_indexes_size('bgt_put')) as indexes_size, count(*) as ct from bgt_put
 UNION ALL 
select 'bgt_scheiding' as table, pg_size_pretty(pg_table_size('bgt_scheiding')) as table_size, pg_size_pretty(pg_indexes_size('bgt_scheiding')) as indexes_size, count(*) as ct from bgt_scheiding
 UNION ALL 
select 'bgt_sensor' as table, pg_size_pretty(pg_table_size('bgt_sensor')) as table_size, pg_size_pretty(pg_indexes_size('bgt_sensor')) as indexes_size, count(*) as ct from bgt_sensor
 UNION ALL 
select 'bgt_spoor' as table, pg_size_pretty(pg_table_size('bgt_spoor')) as table_size, pg_size_pretty(pg_indexes_size('bgt_spoor')) as indexes_size, count(*) as ct from bgt_spoor
 UNION ALL 
select 'bgt_stadsdeel' as table, pg_size_pretty(pg_table_size('bgt_stadsdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_stadsdeel')) as indexes_size, count(*) as ct from bgt_stadsdeel
 UNION ALL 
select 'bgt_straatmeubilair' as table, pg_size_pretty(pg_table_size('bgt_straatmeubilair')) as table_size, pg_size_pretty(pg_indexes_size('bgt_straatmeubilair')) as indexes_size, count(*) as ct from bgt_straatmeubilair
 UNION ALL 
select 'bgt_tunneldeel' as table, pg_size_pretty(pg_table_size('bgt_tunneldeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_tunneldeel')) as indexes_size, count(*) as ct from bgt_tunneldeel
 UNION ALL 
select 'bgt_vegetatieobject' as table, pg_size_pretty(pg_table_size('bgt_vegetatieobject')) as table_size, pg_size_pretty(pg_indexes_size('bgt_vegetatieobject')) as indexes_size, count(*) as ct from bgt_vegetatieobject
 UNION ALL 
select 'bgt_waterdeel' as table, pg_size_pretty(pg_table_size('bgt_waterdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_waterdeel')) as indexes_size, count(*) as ct from bgt_waterdeel
 UNION ALL 
select 'bgt_waterinrichtingselement' as table, pg_size_pretty(pg_table_size('bgt_waterinrichtingselement')) as table_size, pg_size_pretty(pg_indexes_size('bgt_waterinrichtingselement')) as indexes_size, count(*) as ct from bgt_waterinrichtingselement
 UNION ALL 
select 'bgt_waterschap' as table, pg_size_pretty(pg_table_size('bgt_waterschap')) as table_size, pg_size_pretty(pg_indexes_size('bgt_waterschap')) as indexes_size, count(*) as ct from bgt_waterschap
 UNION ALL 
select 'bgt_wegdeel' as table, pg_size_pretty(pg_table_size('bgt_wegdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_wegdeel')) as indexes_size, count(*) as ct from bgt_wegdeel
 UNION ALL 
select 'bgt_weginrichtingselement' as table, pg_size_pretty(pg_table_size('bgt_weginrichtingselement')) as table_size, pg_size_pretty(pg_indexes_size('bgt_weginrichtingselement')) as indexes_size, count(*) as ct from bgt_weginrichtingselement
 UNION ALL 
select 'bgt_wijk' as table, pg_size_pretty(pg_table_size('bgt_wijk')) as table_size, pg_size_pretty(pg_indexes_size('bgt_wijk')) as indexes_size, count(*) as ct from bgt_wijk) as stats ORDER BY stats.ct;" > post-stats.sql
psql -d geodata -f post-stats.sql
