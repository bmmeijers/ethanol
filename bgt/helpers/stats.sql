SELECT * FROM (select 'bgt_bak' as table, pg_size_pretty(pg_table_size('bgt_bak')) as table_size, pg_size_pretty(pg_indexes_size('bgt_bak')) as indexes_size, count(*) as ct from bgt_bak
 UNION ALL 
select 'bgt_begroeidterreindeel' as table, pg_size_pretty(pg_table_size('bgt_begroeidterreindeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_begroeidterreindeel')) as indexes_size, count(*) as ct from bgt_begroeidterreindeel
 UNION ALL 
select 'bgt_bord' as table, pg_size_pretty(pg_table_size('bgt_bord')) as table_size, pg_size_pretty(pg_indexes_size('bgt_bord')) as indexes_size, count(*) as ct from bgt_bord
 UNION ALL 
select 'bgt_buurt' as table, pg_size_pretty(pg_table_size('bgt_buurt')) as table_size, pg_size_pretty(pg_indexes_size('bgt_buurt')) as indexes_size, count(*) as ct from bgt_buurt
 UNION ALL 
select 'bgt_functioneelgebied' as table, pg_size_pretty(pg_table_size('bgt_functioneelgebied')) as table_size, pg_size_pretty(pg_indexes_size('bgt_functioneelgebied')) as indexes_size, count(*) as ct from bgt_functioneelgebied
 UNION ALL 
select 'bgt_gebouwinstallatie' as table, pg_size_pretty(pg_table_size('bgt_gebouwinstallatie')) as table_size, pg_size_pretty(pg_indexes_size('bgt_gebouwinstallatie')) as indexes_size, count(*) as ct from bgt_gebouwinstallatie
 UNION ALL 
select 'bgt_installatie' as table, pg_size_pretty(pg_table_size('bgt_installatie')) as table_size, pg_size_pretty(pg_indexes_size('bgt_installatie')) as indexes_size, count(*) as ct from bgt_installatie
 UNION ALL 
select 'bgt_kast' as table, pg_size_pretty(pg_table_size('bgt_kast')) as table_size, pg_size_pretty(pg_indexes_size('bgt_kast')) as indexes_size, count(*) as ct from bgt_kast
 UNION ALL 
select 'bgt_kunstwerkdeel' as table, pg_size_pretty(pg_table_size('bgt_kunstwerkdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_kunstwerkdeel')) as indexes_size, count(*) as ct from bgt_kunstwerkdeel
 UNION ALL 
select 'bgt_mast' as table, pg_size_pretty(pg_table_size('bgt_mast')) as table_size, pg_size_pretty(pg_indexes_size('bgt_mast')) as indexes_size, count(*) as ct from bgt_mast
 UNION ALL 
select 'bgt_onbegroeidterreindeel' as table, pg_size_pretty(pg_table_size('bgt_onbegroeidterreindeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_onbegroeidterreindeel')) as indexes_size, count(*) as ct from bgt_onbegroeidterreindeel
 UNION ALL 
select 'bgt_ondersteunendwaterdeel' as table, pg_size_pretty(pg_table_size('bgt_ondersteunendwaterdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_ondersteunendwaterdeel')) as indexes_size, count(*) as ct from bgt_ondersteunendwaterdeel
 UNION ALL 
select 'bgt_ondersteunendwegdeel' as table, pg_size_pretty(pg_table_size('bgt_ondersteunendwegdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_ondersteunendwegdeel')) as indexes_size, count(*) as ct from bgt_ondersteunendwegdeel
 UNION ALL 
select 'bgt_ongeclassificeerdobject' as table, pg_size_pretty(pg_table_size('bgt_ongeclassificeerdobject')) as table_size, pg_size_pretty(pg_indexes_size('bgt_ongeclassificeerdobject')) as indexes_size, count(*) as ct from bgt_ongeclassificeerdobject
 UNION ALL 
select 'bgt_openbareruimte' as table, pg_size_pretty(pg_table_size('bgt_openbareruimte')) as table_size, pg_size_pretty(pg_indexes_size('bgt_openbareruimte')) as indexes_size, count(*) as ct from bgt_openbareruimte
 UNION ALL 
select 'bgt_openbareruimtelabel' as table, pg_size_pretty(pg_table_size('bgt_openbareruimtelabel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_openbareruimtelabel')) as indexes_size, count(*) as ct from bgt_openbareruimtelabel
 UNION ALL 
select 'bgt_overbruggingsdeel' as table, pg_size_pretty(pg_table_size('bgt_overbruggingsdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_overbruggingsdeel')) as indexes_size, count(*) as ct from bgt_overbruggingsdeel
 UNION ALL 
select 'bgt_overigbouwwerk' as table, pg_size_pretty(pg_table_size('bgt_overigbouwwerk')) as table_size, pg_size_pretty(pg_indexes_size('bgt_overigbouwwerk')) as indexes_size, count(*) as ct from bgt_overigbouwwerk
 UNION ALL 
select 'bgt_overigescheiding' as table, pg_size_pretty(pg_table_size('bgt_overigescheiding')) as table_size, pg_size_pretty(pg_indexes_size('bgt_overigescheiding')) as indexes_size, count(*) as ct from bgt_overigescheiding
 UNION ALL 
select 'bgt_paal' as table, pg_size_pretty(pg_table_size('bgt_paal')) as table_size, pg_size_pretty(pg_indexes_size('bgt_paal')) as indexes_size, count(*) as ct from bgt_paal
 UNION ALL 
select 'bgt_pand' as table, pg_size_pretty(pg_table_size('bgt_pand')) as table_size, pg_size_pretty(pg_indexes_size('bgt_pand')) as indexes_size, count(*) as ct from bgt_pand
 UNION ALL 
select 'bgt_plaatsbepalingspunt' as table, pg_size_pretty(pg_table_size('bgt_plaatsbepalingspunt')) as table_size, pg_size_pretty(pg_indexes_size('bgt_plaatsbepalingspunt')) as indexes_size, count(*) as ct from bgt_plaatsbepalingspunt
 UNION ALL 
select 'bgt_put' as table, pg_size_pretty(pg_table_size('bgt_put')) as table_size, pg_size_pretty(pg_indexes_size('bgt_put')) as indexes_size, count(*) as ct from bgt_put
 UNION ALL 
select 'bgt_scheiding' as table, pg_size_pretty(pg_table_size('bgt_scheiding')) as table_size, pg_size_pretty(pg_indexes_size('bgt_scheiding')) as indexes_size, count(*) as ct from bgt_scheiding
 UNION ALL 
select 'bgt_sensor' as table, pg_size_pretty(pg_table_size('bgt_sensor')) as table_size, pg_size_pretty(pg_indexes_size('bgt_sensor')) as indexes_size, count(*) as ct from bgt_sensor
 UNION ALL 
select 'bgt_spoor' as table, pg_size_pretty(pg_table_size('bgt_spoor')) as table_size, pg_size_pretty(pg_indexes_size('bgt_spoor')) as indexes_size, count(*) as ct from bgt_spoor
 UNION ALL 
select 'bgt_stadsdeel' as table, pg_size_pretty(pg_table_size('bgt_stadsdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_stadsdeel')) as indexes_size, count(*) as ct from bgt_stadsdeel
 UNION ALL 
select 'bgt_straatmeubilair' as table, pg_size_pretty(pg_table_size('bgt_straatmeubilair')) as table_size, pg_size_pretty(pg_indexes_size('bgt_straatmeubilair')) as indexes_size, count(*) as ct from bgt_straatmeubilair
 UNION ALL 
select 'bgt_tunneldeel' as table, pg_size_pretty(pg_table_size('bgt_tunneldeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_tunneldeel')) as indexes_size, count(*) as ct from bgt_tunneldeel
 UNION ALL 
select 'bgt_vegetatieobject' as table, pg_size_pretty(pg_table_size('bgt_vegetatieobject')) as table_size, pg_size_pretty(pg_indexes_size('bgt_vegetatieobject')) as indexes_size, count(*) as ct from bgt_vegetatieobject
 UNION ALL 
select 'bgt_waterdeel' as table, pg_size_pretty(pg_table_size('bgt_waterdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_waterdeel')) as indexes_size, count(*) as ct from bgt_waterdeel
 UNION ALL 
select 'bgt_waterinrichtingselement' as table, pg_size_pretty(pg_table_size('bgt_waterinrichtingselement')) as table_size, pg_size_pretty(pg_indexes_size('bgt_waterinrichtingselement')) as indexes_size, count(*) as ct from bgt_waterinrichtingselement
 UNION ALL 
select 'bgt_waterschap' as table, pg_size_pretty(pg_table_size('bgt_waterschap')) as table_size, pg_size_pretty(pg_indexes_size('bgt_waterschap')) as indexes_size, count(*) as ct from bgt_waterschap
 UNION ALL 
select 'bgt_wegdeel' as table, pg_size_pretty(pg_table_size('bgt_wegdeel')) as table_size, pg_size_pretty(pg_indexes_size('bgt_wegdeel')) as indexes_size, count(*) as ct from bgt_wegdeel
 UNION ALL 
select 'bgt_weginrichtingselement' as table, pg_size_pretty(pg_table_size('bgt_weginrichtingselement')) as table_size, pg_size_pretty(pg_indexes_size('bgt_weginrichtingselement')) as indexes_size, count(*) as ct from bgt_weginrichtingselement
 UNION ALL 
select 'bgt_wijk' as table, pg_size_pretty(pg_table_size('bgt_wijk')) as table_size, pg_size_pretty(pg_indexes_size('bgt_wijk')) as indexes_size, count(*) as ct from bgt_wijk) as stats ORDER BY stats.ct;
