tpl = "select '{table_nm}' as table, pg_size_pretty(pg_table_size('{table_nm}')) as table_size, pg_size_pretty(pg_indexes_size('{table_nm}')) as indexes_size, count(*) as ct from {table_nm}"
sql = []
for table_nm in sorted(["bgt_wegdeel",
    "bgt_ondersteunendwegdeel",
    "bgt_onbegroeidterreindeel",
    "bgt_begroeidterreindeel",
    "bgt_waterdeel",
    "bgt_ondersteunendwaterdeel",
    "bgt_pand",
    "bgt_spoor",
    "bgt_overigbouwwerk",
    "bgt_kunstwerkdeel",
    "bgt_overbruggingsdeel",
    "bgt_tunneldeel",
    "bgt_scheiding",
    "bgt_overigescheiding",
    "bgt_functioneelgebied",
    "bgt_ongeclassificeerdobject",
    "bgt_openbareruimtelabel",
    "bgt_plaatsbepalingspunt",
    "bgt_gebouwinstallatie",
    "bgt_bak",
    "bgt_bord",
    "bgt_installatie",
    "bgt_kast",
    "bgt_mast",
    "bgt_paal",
    "bgt_put",
    "bgt_sensor",
    "bgt_straatmeubilair",
    "bgt_waterinrichtingselement",
    "bgt_weginrichtingselement",
    "bgt_vegetatieobject",
    "bgt_buurt",
    "bgt_openbareruimte",
    "bgt_stadsdeel",
    "bgt_waterschap",
    "bgt_wijk"]):
    sql.append(tpl.format(table_nm=table_nm))

print("SELECT * FROM ({}) as stats ORDER BY stats.ct;".format("\n UNION ALL \n".join(sql)))
