VACUUM ANALYZE "martijn"."bgt_wegdeel";
VACUUM ANALYZE "martijn"."bgt_ondersteunendwegdeel";
VACUUM ANALYZE "martijn"."bgt_onbegroeidterreindeel";
VACUUM ANALYZE "martijn"."bgt_begroeidterreindeel";
VACUUM ANALYZE "martijn"."bgt_waterdeel";
VACUUM ANALYZE "martijn"."bgt_ondersteunendwaterdeel";
VACUUM ANALYZE "martijn"."bgt_pand";
VACUUM ANALYZE "martijn"."bgt_spoor";
VACUUM ANALYZE "martijn"."bgt_overigbouwwerk";
VACUUM ANALYZE "martijn"."bgt_kunstwerkdeel";
VACUUM ANALYZE "martijn"."bgt_overbruggingsdeel";
VACUUM ANALYZE "martijn"."bgt_tunneldeel";
VACUUM ANALYZE "martijn"."bgt_scheiding";
VACUUM ANALYZE "martijn"."bgt_overigescheiding";
VACUUM ANALYZE "martijn"."bgt_functioneelgebied";
VACUUM ANALYZE "martijn"."bgt_ongeclassificeerdobject";
VACUUM ANALYZE "martijn"."bgt_openbareruimtelabel";
VACUUM ANALYZE "martijn"."bgt_plaatsbepalingspunt";
VACUUM ANALYZE "martijn"."bgt_gebouwinstallatie";
VACUUM ANALYZE "martijn"."bgt_bak";
VACUUM ANALYZE "martijn"."bgt_bord";
VACUUM ANALYZE "martijn"."bgt_installatie";
VACUUM ANALYZE "martijn"."bgt_kast";
VACUUM ANALYZE "martijn"."bgt_mast";
VACUUM ANALYZE "martijn"."bgt_paal";
VACUUM ANALYZE "martijn"."bgt_put";
VACUUM ANALYZE "martijn"."bgt_sensor";
VACUUM ANALYZE "martijn"."bgt_straatmeubilair";
VACUUM ANALYZE "martijn"."bgt_waterinrichtingselement";
VACUUM ANALYZE "martijn"."bgt_weginrichtingselement";
VACUUM ANALYZE "martijn"."bgt_vegetatieobject";
VACUUM ANALYZE "martijn"."bgt_buurt";
VACUUM ANALYZE "martijn"."bgt_openbareruimte";
VACUUM ANALYZE "martijn"."bgt_stadsdeel";
VACUUM ANALYZE "martijn"."bgt_waterschap";
VACUUM ANALYZE "martijn"."bgt_wijk";
