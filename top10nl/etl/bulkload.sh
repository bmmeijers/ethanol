#!/bin/bash

timestamp() {
  local d=`date +"%T"`
  echo '--' $d '--' $1
}

# wget filechunks from pdok
# unzip filechunks zip
# place bulkload.sh + top10-v1.2.gfs in folder with chunks of gml

timestamp 'begin load'

echo '<?xml version="1.0" encoding="UTF-8"?><gml:FeatureCollection xmlns:gml="http://www.opengis.net/gml"><gml:featureMember><object /></gml:featureMember></gml:FeatureCollection>' > _prepare.gml
cp top10-v1.2.gfs _prepare.gfs
ogr2ogr --config PG_USE_COPY YES -f "PGDUMP" _prepare.sql _prepare.gml -lco SPATIAL_INDEX=ON -lco SCHEMA=martijn 
# -lco GEOMETRY_NAME=geometrie_vlak

# split out in non-spatial index / spatial index
grep -v "USING GIST" _prepare.sql > create-schema.sql


# remove temp files produced by the above
rm _prepare.gml
rm _prepare.gfs
rm _prepare.sql

## disabled creating default indexes as suggested by ogr2ogr
## see below for manual index creation
# grep "USING GIST" _prepare.sql > create-index.sql
## split over files with maximum 3 lines per file
## so that we can 
#split -l 3 _index.sql post-index- --additional-suffix=.sql
#rm _index.sql

timestamp 'begin conversion'

# do the hard work: conversion from GML to PGDUMP format
find . -name '*.gml' | parallel cp top10-v1.2.gfs {.}.gfs
# make sure files of gfs are newer than gml files
touch *.gml
sleep 2
touch *.gfs
# convert the gml file
find . -name '*.gml' | parallel ogr2ogr -f "PGDUMP" {.}.sql --config PG_USE_COPY YES {} -lco SPATIAL_INDEX=NO -lco CREATE_TABLE=NO -lco SCHEMA=martijn -skipfailures -append -update

timestamp 'loading schema'

# load: create schema
psql -d geodata -f create-schema.sql

timestamp 'loading data'

# load: data
find . -name 'Top10NL*.sql' | parallel psql -d geodata -f {}

timestamp 'loading indexes'

# SET default_tablespace indx;
# make sure that the multiple of the number of lines is corresponding with how split is instructed to split the files...


echo 'SET default_tablespace=INDX;
CREATE INDEX "t10_functioneelgebied_geometrie_vlak_geom_idx" ON "martijn"."t10_functioneelgebied" USING GIST ("geometrie_vlak");
CLUSTER "martijn"."t10_functioneelgebied" USING "t10_functioneelgebied_geometrie_vlak_geom_idx";
CREATE INDEX "t10_functioneelgebied_geometrie_multivlak_geom_idx" ON "martijn"."t10_functioneelgebied" USING GIST ("geometrie_multivlak");
CREATE INDEX "t10_functioneelgebied_geometrie_punt_geom_idx" ON "martijn"."t10_functioneelgebied" USING GIST ("geometrie_punt");





SET default_tablespace=INDX;
CREATE INDEX "t10_gebouw_geometrie_vlak_geom_idx" ON "martijn"."t10_gebouw" USING GIST ("geometrie_vlak");
CLUSTER "martijn"."t10_gebouw" USING "t10_gebouw_geometrie_vlak_geom_idx";
CREATE INDEX "t10_gebouw_geometrie_punt_geom_idx" ON "martijn"."t10_gebouw" USING GIST ("geometrie_punt");
CREATE INDEX "t10_gebouw_visualisatiecode_idx" ON "martijn"."t10_gebouw" ("visualisatiecode");





SET default_tablespace=INDX;
CREATE INDEX "t10_geografischgebied_geometrie_vlak_geom_idx" ON "martijn"."t10_geografischgebied" USING GIST ("geometrie_vlak");
CLUSTER "martijn"."t10_geografischgebied" USING "t10_geografischgebied_geometrie_vlak_geom_idx";
CREATE INDEX "t10_geografischgebied_geometrie_multivlak_geom_idx" ON "martijn"."t10_geografischgebied" USING GIST ("geometrie_multivlak");
CREATE INDEX "t10_geografischgebied_geometrie_punt_geom_idx" ON "martijn"."t10_geografischgebied" USING GIST ("geometrie_punt");





SET default_tablespace=INDX;
CREATE INDEX "t10_hoogte_geometrie_lijn_geom_idx" ON "martijn"."t10_hoogte" USING GIST ("geometrie_lijn");
CLUSTER "martijn"."t10_hoogte" USING "t10_hoogte_geometrie_lijn_geom_idx";
CREATE INDEX "t10_hoogte_geometrie_punt_geom_idx" ON "martijn"."t10_hoogte" USING GIST ("geometrie_punt");






SET default_tablespace=INDX;
CREATE INDEX "t10_inrichtingselement_geometrie_lijn_geom_idx" ON "martijn"."t10_inrichtingselement" USING GIST ("geometrie_lijn");
CLUSTER "martijn"."t10_inrichtingselement" USING "t10_inrichtingselement_geometrie_lijn_geom_idx";
CREATE INDEX "t10_inrichtingselement_geometrie_punt_geom_idx" ON "martijn"."t10_inrichtingselement" USING GIST ("geometrie_punt");






SET default_tablespace=INDX;
CREATE INDEX "t10_plaats_geometrie_vlak_geom_idx" ON "martijn"."t10_plaats" USING GIST ("geometrie_vlak");
CLUSTER "martijn"."t10_plaats" USING "t10_plaats_geometrie_vlak_geom_idx";
CREATE INDEX "t10_plaats_geometrie_multivlak_geom_idx" ON "martijn"."t10_plaats" USING GIST ("geometrie_multivlak");
CREATE INDEX "t10_plaats_geometrie_punt_geom_idx" ON "martijn"."t10_plaats" USING GIST ("geometrie_punt");





SET default_tablespace=INDX;
CREATE INDEX "t10_plantopografie_geometrie_vlak_geom_idx" ON "martijn"."t10_plantopografie" USING GIST ("geometrie_vlak");
CLUSTER "martijn"."t10_plantopografie" USING "t10_plantopografie_geometrie_vlak_geom_idx";
CREATE INDEX "t10_plantopografie_geometrie_lijn_geom_idx" ON "martijn"."t10_plantopografie" USING GIST ("geometrie_lijn");
CREATE INDEX "t10_plantopografie_geometrie_punt_geom_idx" ON "martijn"."t10_plantopografie" USING GIST ("geometrie_punt");





SET default_tablespace=INDX;
CREATE INDEX "t10_registratiefgebied_geometrie_vlak_geom_idx" ON "martijn"."t10_registratiefgebied" USING GIST ("geometrie_vlak");
CLUSTER "martijn"."t10_registratiefgebied" USING "t10_registratiefgebied_geometrie_vlak_geom_idx";
CREATE INDEX "t10_registratiefgebied_geometrie_multivlak_geom_idx" ON "martijn"."t10_registratiefgebied" USING GIST ("geometrie_multivlak");






SET default_tablespace=INDX;
CREATE INDEX "t10_relief_geometrie_lijn_geom_idx" ON "martijn"."t10_relief" USING GIST ("geometrie_lijn");
CLUSTER "martijn"."t10_relief" USING "t10_relief_geometrie_lijn_geom_idx";
CREATE INDEX "t10_relief_geometrie_hogezijde_geom_idx" ON "martijn"."t10_relief" USING GIST ("geometrie_hogezijde");
CREATE INDEX "t10_relief_geometrie_lagezijde_geom_idx" ON "martijn"."t10_relief" USING GIST ("geometrie_lagezijde");





SET default_tablespace=INDX;
CREATE INDEX "t10_spoorbaandeel_geometrie_lijn_geom_idx" ON "martijn"."t10_spoorbaandeel" USING GIST ("geometrie_lijn");
CLUSTER "martijn"."t10_spoorbaandeel" USING "t10_spoorbaandeel_geometrie_lijn_geom_idx";
CREATE INDEX "t10_spoorbaandeel_geometrie_punt_geom_idx" ON "martijn"."t10_spoorbaandeel" USING GIST ("geometrie_punt");






SET default_tablespace=INDX;
ALTER TABLE "martijn"."t10_terrein" RENAME COLUMN wkb_geometry TO geometrie_vlak;
CREATE INDEX "t10_terrein_geometrie_vlak_geom_idx" ON "martijn"."t10_terrein" USING GIST ("geometrie_vlak");
CLUSTER "martijn"."t10_terrein" USING "t10_terrein_geometrie_vlak_geom_idx";
CREATE INDEX "t10_terrein_visualisatiecode_idx" ON "martijn"."t10_terrein" ("visualisatiecode");





SET default_tablespace=INDX;
CREATE INDEX "t10_waterdeel_geometrie_vlak_geom_idx" ON "martijn"."t10_waterdeel" USING GIST ("geometrie_vlak");
CLUSTER "martijn"."t10_waterdeel" USING "t10_waterdeel_geometrie_vlak_geom_idx";
CREATE INDEX "t10_waterdeel_geometrie_lijn_geom_idx" ON "martijn"."t10_waterdeel" USING GIST ("geometrie_lijn");
CREATE INDEX "t10_waterdeel_geometrie_punt_geom_idx" ON "martijn"."t10_waterdeel" USING GIST ("geometrie_punt");
CREATE INDEX "t10_waterdeel_visualisatiecode_idx" ON "martijn"."t10_waterdeel" ("visualisatiecode");




SET default_tablespace=INDX;
CREATE INDEX "t10_wegdeel_geometrie_vlak_geom_idx" ON "martijn"."t10_wegdeel" USING GIST ("geometrie_vlak");
CLUSTER "martijn"."t10_wegdeel" USING "t10_wegdeel_geometrie_vlak_geom_idx";
CREATE INDEX "t10_wegdeel_geometrie_lijn_geom_idx" ON "martijn"."t10_wegdeel" USING GIST ("geometrie_lijn");
CREATE INDEX "t10_wegdeel_geometrie_punt_geom_idx" ON "martijn"."t10_wegdeel" USING GIST ("geometrie_punt");
CREATE INDEX "t10_wegdeel_geometrie_hartpunt_geom_idx" ON "martijn"."t10_wegdeel" USING GIST ("geometrie_hartpunt");
CREATE INDEX "t10_wegdeel_geometrie_hartlijn_geom_idx" ON "martijn"."t10_wegdeel" USING GIST ("geometrie_hartlijn");
CREATE INDEX "t10_wegdeel_visualisatiecode_idx" ON "martijn"."t10_wegdeel" ("visualisatiecode");


' > _index.sql

# split the file into more files with "-l X" lines per file
split -l 10 _index.sql post-index- --additional-suffix=.sql
# create the indexes per feature type in parallel
find . -name 'post-index-*.sql' | parallel psql -d geodata -f {}

timestamp 'vacuum tables (background)'

echo 'VACUUM ANALYZE "martijn"."t10_functioneelgebied";
VACUUM ANALYZE "martijn"."t10_gebouw";
VACUUM ANALYZE "martijn"."t10_geografischgebied";
VACUUM ANALYZE "martijn"."t10_hoogte";
VACUUM ANALYZE "martijn"."t10_inrichtingselement";
VACUUM ANALYZE "martijn"."t10_plaats";
VACUUM ANALYZE "martijn"."t10_plantopografie";
VACUUM ANALYZE "martijn"."t10_registratiefgebied";
VACUUM ANALYZE "martijn"."t10_relief";
VACUUM ANALYZE "martijn"."t10_spoorbaandeel";
VACUUM ANALYZE "martijn"."t10_terrein";
VACUUM ANALYZE "martijn"."t10_waterdeel";
VACUUM ANALYZE "martijn"."t10_wegdeel";' > post-vacuum.sql
nohup psql -d geodata -f post-vacuum.sql &

# python code for obtaining query:
#tpl = "select '{table_nm}' as table, pg_size_pretty(pg_indexes_size('{table_nm}')) as index_size, pg_size_pretty(pg_table_size('{table_nm}')) as table_size, count(*) from {table_nm}"
#sql = []
#for table_nm in ["t10_functioneelgebied", "t10_gebouw", "t10_geografischgebied", "t10_hoogte", "t10_inrichtingselement", "t10_plaats", "t10_plantopografie", "t10_registratiefgebied", "t10_relief", "t10_spoorbaandeel", "t10_terrein", "t10_waterdeel", "t10_wegdeel"]:
#    sql.append(tpl.format(table_nm=table_nm))
#
#print("{};".format("\n UNION ALL \n".join(sql)))

echo "select 't10_functioneelgebied' as table, pg_size_pretty(pg_indexes_size('t10_functioneelgebied')) as index_size, pg_size_pretty(pg_table_size('t10_functioneelgebied')) as table_size, count(*) from t10_functioneelgebied
 UNION ALL 
select 't10_gebouw' as table, pg_size_pretty(pg_indexes_size('t10_gebouw')) as index_size, pg_size_pretty(pg_table_size('t10_gebouw')) as table_size, count(*) from t10_gebouw
 UNION ALL 
select 't10_geografischgebied' as table, pg_size_pretty(pg_indexes_size('t10_geografischgebied')) as index_size, pg_size_pretty(pg_table_size('t10_geografischgebied')) as table_size, count(*) from t10_geografischgebied
 UNION ALL 
select 't10_hoogte' as table, pg_size_pretty(pg_indexes_size('t10_hoogte')) as index_size, pg_size_pretty(pg_table_size('t10_hoogte')) as table_size, count(*) from t10_hoogte
 UNION ALL 
select 't10_inrichtingselement' as table, pg_size_pretty(pg_indexes_size('t10_inrichtingselement')) as index_size, pg_size_pretty(pg_table_size('t10_inrichtingselement')) as table_size, count(*) from t10_inrichtingselement
 UNION ALL 
select 't10_plaats' as table, pg_size_pretty(pg_indexes_size('t10_plaats')) as index_size, pg_size_pretty(pg_table_size('t10_plaats')) as table_size, count(*) from t10_plaats
 UNION ALL 
select 't10_plantopografie' as table, pg_size_pretty(pg_indexes_size('t10_plantopografie')) as index_size, pg_size_pretty(pg_table_size('t10_plantopografie')) as table_size, count(*) from t10_plantopografie
 UNION ALL 
select 't10_registratiefgebied' as table, pg_size_pretty(pg_indexes_size('t10_registratiefgebied')) as index_size, pg_size_pretty(pg_table_size('t10_registratiefgebied')) as table_size, count(*) from t10_registratiefgebied
 UNION ALL 
select 't10_relief' as table, pg_size_pretty(pg_indexes_size('t10_relief')) as index_size, pg_size_pretty(pg_table_size('t10_relief')) as table_size, count(*) from t10_relief
 UNION ALL 
select 't10_spoorbaandeel' as table, pg_size_pretty(pg_indexes_size('t10_spoorbaandeel')) as index_size, pg_size_pretty(pg_table_size('t10_spoorbaandeel')) as table_size, count(*) from t10_spoorbaandeel
 UNION ALL 
select 't10_terrein' as table, pg_size_pretty(pg_indexes_size('t10_terrein')) as index_size, pg_size_pretty(pg_table_size('t10_terrein')) as table_size, count(*) from t10_terrein
 UNION ALL 
select 't10_waterdeel' as table, pg_size_pretty(pg_indexes_size('t10_waterdeel')) as index_size, pg_size_pretty(pg_table_size('t10_waterdeel')) as table_size, count(*) from t10_waterdeel
 UNION ALL 
select 't10_wegdeel' as table, pg_size_pretty(pg_indexes_size('t10_wegdeel')) as index_size, pg_size_pretty(pg_table_size('t10_wegdeel')) as table_size, count(*) from t10_wegdeel;" > post-stats.sql

psql -d geodata -f post-stats.sql

timestamp 'done'
