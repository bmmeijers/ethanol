tpl = "select '{table_nm}' as table, pg_size_pretty(pg_indexes_size('{table_nm}')) as index_size, pg_size_pretty(pg_table_size('{table_nm}')) as table_size, count(*) from {table_nm}"
sql = []
for table_nm in sorted(["t10_functioneelgebied", "t10_gebouw", "t10_geografischgebied", "t10_hoogte", "t10_inrichtingselement", "t10_plaats", "t10_plantopografie", "t10_registratiefgebied", "t10_relief", "t10_spoorbaandeel", "t10_terrein", "t10_waterdeel", "t10_wegdeel"]):
    sql.append(tpl.format(table_nm=table_nm))

print("{};".format("\n UNION ALL \n".join(sql)))
